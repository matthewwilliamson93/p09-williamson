package edu.binghamton.cs.hello;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

public class FontSprite extends Sprite {
	private String text;
	private Paint borderPaint;

	public FontSprite(Context context) {
		super(context);
		this.paint.setColor(Color.WHITE);
		this.paint.setTypeface(Typeface.DEFAULT_BOLD);
		this.paint.setTextAlign(Paint.Align.LEFT);
		this.text("");
		this.size(40);
		this.borderPaint = new Paint(this.paint);
		this.borderPaint.setStyle(Paint.Style.STROKE);
		this.borderPaint.setStrokeWidth(12);
		this.borderPaint.setColor(Color.BLACK);
	}

	public FontSprite(Context context, String text) {
		this(context);
		this.text = text;
	}

	@Override
	public void draw(Canvas canvas) {
		this.borderPaint.setAlpha(this.paint.getAlpha());
		canvas.drawText(this.text, (int) this.x, (int) this.y, this.borderPaint);
		canvas.drawText(this.text, (int) this.x, (int) this.y, this.paint);
	}

	public void text(String text) {
		this.text = text;
	}

	public String text() {
		return this.text;
	}

	public float size() {
		return this.paint.getTextSize() / 3.0f;
	}

	public void size(float nsize) {
		this.paint.setTextSize(nsize);
		this.paint.setTextSize(this.paint.getTextSize() * 3.0f);
	}
}