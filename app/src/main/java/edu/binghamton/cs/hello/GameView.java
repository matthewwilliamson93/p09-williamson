package edu.binghamton.cs.hello;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GameView extends View {
	final static float LOG_POP_START = 80.0f;
	final static float LOG_POP_SPEED = 10.0f;
	final static float LOG_POP_END = 40.0f;
	final static float LOG_SPACING = 160.0f;
	final static float LOG_START = 800.0f;
	final static float KEYS_START = LOG_START + LOG_SPACING * 2;
	final static float TURN_LENGTH = 3500;

	private enum State {
		ADD_PLAYER, ADD_NAME, PLAY, END
	}

	Timer timer;
	Context context;
	ResourceManager res;
	Random rng;
	LinkedList<FontSprite> log;
	ArrayList<FontSprite> keys;
	FontSprite[][] board;
	Boolean[][] hide;
	String[][] dies = {
			{"R", "I", "F", "O", "B", "X"},
			{"I", "F", "E", "H", "E", "Y"},
			{"D", "E", "N", "O", "W", "S"},
			{"U", "T", "O", "K", "N", "D"},
			{"H", "M", "S", "R", "A", "O"},
			{"L", "U", "P", "E", "T", "S"},
			{"A", "C", "I", "T", "O", "A"},
			{"Y", "L", "G", "K", "U", "E"},
			{"QU", "B", "M", "J", "O", "A"},
			{"E", "H", "I", "S", "P", "N"},
			{"V", "E", "T", "I", "G", "N"},
			{"B", "A", "L", "I", "Y", "T"},
			{"E", "Z", "A", "V", "N", "D"},
			{"R", "A", "L", "E", "S", "C"},
			{"U", "W", "I", "L", "R", "G"},
			{"P", "A", "C", "E", "M", "D"}
	};
	ArrayList<Player> players;
	ArrayList<Player> out;

	FontSprite banner;
	FontSprite add;
	FontSprite name;

	String word;
	State state;
	int turn;
	Trie dict;
	HashSet<String> used;
	int curX;
	int curY;
	Paint positionPaint;

	public GameView(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		this.context = context;
		this.res = new ResourceManager(context);
		// Global state
		this.rng = new Random();
		this.dict = new Trie();
		this.used = new HashSet<>();
		this.log = new LinkedList<>();
		this.keys = new ArrayList<>();
		this.board = new FontSprite[4][4];
		this.hide = new Boolean[4][4];
		this.positionPaint = new Paint();
		this.positionPaint.setColor(Color.CYAN);
		this.players = new ArrayList<>();
		this.out = new ArrayList<>();
		this.banner = new FontSprite(this.context, "Woggle");
		this.banner.move(325, 160);
		this.add = new FontSprite(this.context, "Add players?");
		this.add.move(10, KEYS_START);
		this.name = new FontSprite(this.context, "");
		this.name.move(10, KEYS_START - LOG_SPACING);
		// Setup keyboard
		for (char let = 'A'; let <= '['; let++) {
			FontSprite f = new FontSprite(this.context, "" + let);
			int num = let - 'A';
			if (num < 21) {
				int x = num % 7;
				int y = num / 7;
				f.move(x * LOG_SPACING + LOG_SPACING / 2.0f - 50, y * LOG_SPACING + KEYS_START);
			} else {
				int x = num - 21;
				f.move(x * LOG_SPACING + LOG_SPACING - 50, 3 * LOG_SPACING + KEYS_START);
			}
			this.keys.add(f);
		}
		this.keys.get(this.keys.size() - 1).text("-");
		// Setup board
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				this.board[i][j] = new FontSprite(this.context, "");
				this.board[i][j].move(i * LOG_SPACING + LOG_SPACING * 1.5f, KEYS_START + j * LOG_SPACING);
			}
		}
		reset();
		// Populate the dict
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(context.getAssets().open("dict.txt")));
			String w;
			while ((w = br.readLine()) != null) {
				this.dict.insert(w);
			}
			br.close();
		} catch (Exception e) {
			// Well the file is always there so ... stop being drunk Android
		}
		// Set up interval timer
		this.timer = new Timer();
		this.timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				GameView.this.tick();
				GameView.this.postInvalidate();
			}
		}, 0, 16);
	}

	private void reset() {
		this.players.clear();
		this.out.clear();
		this.used.clear();
		change(State.ADD_PLAYER);
		this.name.text("");
		this.add.text("Add players?");
		shuffle();
	}

	private void replay() {
		for (Player p : this.out) {
			p.reset();
			this.players.add(p);
		}
		this.used.clear();
		change(State.PLAY);
		shuffle();
	}

	private void shuffle() {
		Collections.shuffle(Arrays.asList(this.dies));
		for (String[] d : this.dies) {
			Collections.shuffle(Arrays.asList(d));
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				this.board[i][j].text(this.dies[i * 4 + j][0]);
			}
		}
		unHide();
	}

	private void unHide() {
		this.curX = -1;
		this.curY = -1;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				this.hide[i][j] = false;
			}
		}
	}

	private void change(State state) {
		this.state = state;
		switch (this.state) {
			case ADD_PLAYER:
				this.name.text("Play!");
				for (FontSprite key : this.keys) {
					key.text(key.text().toUpperCase());
				}
				break;
			case ADD_NAME:
				this.addLine("What name?");
				this.name.text("");
				break;
			case PLAY:
				Player p = this.players.get(this.turn);
				this.name.text(p.name() + ": ");
				this.word = "";
				break;
			case END:
				this.name.text("Play again?");
				this.add.text("Change players?");
				break;
		}
	}

	private void tick() {
		if (this.state == State.PLAY) {
			this.players.get(this.turn).tick();
			if (this.players.get(this.turn).done()) {
				this.out.add(this.players.get(this.turn));
				this.addLine(this.players.get(this.turn).name() + " out of time.");
				this.players.remove(this.turn);
				if (this.players.size() != 0) {
					this.turn = (this.turn + 1) % this.players.size();
					this.name.text(this.players.get(this.turn).name() + ": ");
				} else {
					boolean tie = false;
					Player winner = null;
					for (Player p : this.out) {
						if (winner == null) {
							winner = p;
						} else if (winner.score() < p.score()) {
							winner = p;
							tie = false;
						} else if (winner.score() == p.score()) {
							tie = true;
						}
					}
					if (tie) {
						this.addLine("Its a tie with " + winner.score() + "!");
					} else {
						this.addLine(winner.name() + " wins with " + winner.score() + "!");
					}
					change(State.END);
				}
			}
		}
		synchronized (this) {
			for (FontSprite f : this.log) {
				if (LOG_POP_END + 1.0f < f.size()) {
					float ds = f.size() - LOG_POP_END;
					f.size(f.size() - ds / LOG_POP_SPEED);
				}
			}
		}
	}

	private void addLine(String line) {
		if (this.log.size() == 4) {
			synchronized (this) {
				this.log.removeLast();
			}
		}
		for (FontSprite f : this.log) {
			f.dy(-LOG_SPACING);
		}
		FontSprite f = new FontSprite(this.context, line);
		f.move(0, LOG_START);
		f.size(LOG_POP_START);
		synchronized (this) {
			this.log.addFirst(f);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction() & MotionEvent.ACTION_MASK;
		if (action == MotionEvent.ACTION_DOWN) {
			float x = event.getX();
			float y = event.getY();
			switch (this.state) {
				case ADD_PLAYER:
					addPlayer(y);
					break;
				case ADD_NAME:
					addName(x, y);
					break;
				case PLAY:
					play(x, y);
					break;
				case END:
					end(y);
					break;
			}
		}
		return true;
	}

	private void addPlayer(float y) {
		if (inRange(this.add, y)) {
			change(State.ADD_NAME);
		} else if (0 < this.players.size() && inRange(this.name, y)) {
			this.addLine("Round start!");
			this.turn = this.rng.nextInt(this.players.size());
			this.word = "";
			change(State.PLAY);
		}
	}

	private void addName(float x, float y) {
		if (0 < this.name.text().length() && inRange(this.name, y)) {
			this.addLine("Added " + this.name.text() + ".");
			Player p = new Player(this.context, this.name.text(), TURN_LENGTH);
			p.wheel().move(850, LOG_START + 20);
			this.players.add(p);
			change(State.ADD_PLAYER);
			return;
		}
		for (FontSprite key : this.keys) {
			RectF bounds = new RectF(key.x() - LOG_SPACING / 2,
					key.y() - LOG_SPACING, key.x() + LOG_SPACING / 2, key.y());
			if (bounds.contains(x, y)) {
				if (this.name.text().length() != 0 && key.text().equals("-")) {
					this.name.text(this.name.text().substring(0, this.name.text().length() - 1));
				} else {
					this.name.text(this.name.text() + key.text());
				}
				break;
			}
		}
		if (this.name.text().length() == 0) {
			for (FontSprite key : this.keys) {
				key.text(key.text().toUpperCase());
			}
		}
		if (this.name.text().length() == 1) {
			for (FontSprite key : this.keys) {
				key.text(key.text().toLowerCase());
			}
		}
	}

	private void play(float x, float y) {
		if (this.word.length() != 0 && inRange(this.name, y)) {
			this.addLine(this.word);
			int len = this.word.length();
			if (len < 3) {
				this.addLine("Too short.");
			} else if (!this.dict.contains(this.word)) {
				this.addLine("Not a word.");
			} else if (this.used.contains(this.word)) {
				this.addLine("Already used.");
			} else {
				this.used.add(this.word);
				int points = len < 7 ? len - 2 : 11;
				this.players.get(this.turn).add(points);
				this.addLine("Nice +" + points);
				this.addLine("Total: " + this.players.get(this.turn).score());
			}
			unHide();
			this.turn = (this.turn + 1) % this.players.size();
			this.word = "";
			this.name.text(this.players.get(this.turn).name() + ": ");
			return;
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				FontSprite b = this.board[i][j];
				if (this.hide[i][j]) {
					continue;
				}
				RectF bounds = new RectF(b.x() - LOG_SPACING / 2,
						b.y() - LOG_SPACING, b.x() + LOG_SPACING / 2, b.y());
				if (bounds.contains(x, y) && (this.curX == -1
						|| (Math.abs(this.curX - i) <= 1
						&& Math.abs(this.curY - j) <= 1))) {
					this.curX = i;
					this.curY = j;
					this.hide[i][j] = true;
					this.name.text(this.name.text() + b.text());
					this.word += b.text();
					return;
				}
			}
		}
	}

	private void end(float y) {
		if (inRange(this.name, y)) {
			replay();
		} else if (inRange(this.add, y)) {
			reset();
		}
	}

	private boolean inRange(FontSprite sprite, float y) {
		return sprite.y() - LOG_SPACING < y && y < sprite.y();
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		this.banner.draw(canvas);
		synchronized (this) {
			for (FontSprite l : this.log) {
				l.draw(canvas);
			}
		}
		this.name.draw(canvas);
		switch (this.state) {
			case ADD_PLAYER:
			case END:
				this.add.draw(canvas);
				break;
			case ADD_NAME:
				for (FontSprite k : this.keys) {
					k.draw(canvas);
				}
				break;
			case PLAY:
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						if (!this.hide[i][j]) {
							this.board[i][j].draw(canvas);
						}
					}
				}
				if (this.curX != -1) {
					float x = this.board[this.curX][this.curY].x();
					float y = this.board[this.curX][this.curY].y();
					canvas.drawCircle(x + 45, y - 45, 45, positionPaint);
				}
				this.players.get(this.turn).wheel().draw(canvas);
				break;
		}
	}
}