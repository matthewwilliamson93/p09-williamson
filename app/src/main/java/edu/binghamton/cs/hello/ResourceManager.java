//http://blog.pseudoblue.com/2010/08/15/android-bitmaps-and-memory-leaks/
package edu.binghamton.cs.hello;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

import java.util.HashMap;
import java.util.Map.Entry;

class ResourceManager {
	private Context context;
	private HashMap<Integer, Bitmap> bitmaps;
	private HashMap<Integer, Drawable> drawables;
	private HashMap<String, Typeface> typefaces;
	private boolean active = true;

	public ResourceManager(Context context) {
		this.bitmaps = new HashMap<Integer, Bitmap>();
		this.drawables = new HashMap<Integer, Drawable>();
		this.typefaces = new HashMap<String, Typeface>();
		this.context = context;
	}

	public Bitmap bitmap(int resource) {
		if (!this.active) return null;
		if (!this.bitmaps.containsKey(resource)) {
			Bitmap temp = BitmapFactory.decodeResource(this.context.getResources(), resource);
			this.bitmaps.put(resource, temp);
		}
		return this.bitmaps.get(resource);
	}

	public Drawable drawable(int resource) {
		if (!this.active) return null;
		if (!this.drawables.containsKey(resource)) {
			Drawable temp = this.context.getResources().getDrawable(resource);
			this.drawables.put(resource, temp);
		}
		return this.drawables.get(resource);
	}

	public Typeface typeface(String path) {
		if (!this.active) return null;
		if (!this.typefaces.containsKey(path)) {
			Typeface temp = Typeface.createFromAsset(this.context.getAssets(), path);
			this.typefaces.put(path, temp);
		}
		return this.typefaces.get(path);
	}

	public void recycle() {
		for (Entry<Integer, Bitmap> entry : this.bitmaps.entrySet()) {
			entry.getValue().recycle();
		}
		this.bitmaps.clear();
	}

	public boolean active() {
		return this.active;
	}

	public boolean active(boolean active) {
		this.active = active;
		return this.active;
	}
}