package edu.binghamton.cs.hello;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

public abstract class Sprite {
	protected Context context;
	protected float x, y;
	protected float rotation;
	protected Paint paint;

	public Sprite(Context context) {
		this.context = context;
		this.paint = new Paint();
		this.paint.setAntiAlias(true);
		this.paint.setFilterBitmap(true);
		this.paint.setDither(true);
		this.paint.setColor(0xFFFF9999);
	}

	public void alpha(int alpha) {
		this.paint.setAlpha(alpha);
	}

	public int alpha() {
		return this.paint.getAlpha();
	}

	public void dalpha(int dalpha) {
		int newVal = this.paint.getAlpha() + dalpha;
		if (newVal < 0) {
			newVal = 0;
		} else if (newVal > 255) {
			newVal = 255;
		}
		this.paint.setAlpha(newVal);
	}

	// set both coordinates
	public void move(float x, float y) {
		this.x = x;
		this.y = y;
	}

	// adjust both coordinates
	public void offset(float dx, float dy) {
		this.move(this.x + dx, this.y + dy);
	}

	// set a single coordinate
	public void x(float x) {
		this.move(x, this.y);
	}

	public void y(float y) {
		this.move(this.x, y);
	}

	// adjust a coordinate
	public float dx(float dx) {
		this.move(this.x + dx, this.y);
		return this.y;
	}

	public float dy(float dy) {
		this.move(this.x, this.y + dy);
		return this.y;
	}

	// get a coordinate
	public float x() {
		return this.x;
	}

	public float y() {
		return this.y;
	}

	public void rotation(float rotation) {
		this.rotation = rotation;
	}

	public float rotation() {
		return this.rotation;
	}

	public Paint paint() {
		return this.paint;
	}

	abstract void draw(Canvas canvas);
}