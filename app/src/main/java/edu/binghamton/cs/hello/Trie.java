package edu.binghamton.cs.hello;

import java.util.HashMap;

/**
 * Trie class is a classic trie implementation
 * Used to act like an efficient dictionary for the AI
 *
 * @author Matthew Williamson
 */
public class Trie {
	public char c;
	public HashMap<Character, Trie> children;
	public boolean isLeaf;

	/**
	 * Default Constructor for the trie
	 */
	public Trie() {
		this.children = new HashMap<>();
		this.isLeaf = false;
	}

	/**
	 * Explicit Value Constructor for the trie
	 *
	 * @param c The character to be stored in the node
	 */
	public Trie(char c) {
		this.c = c;
		this.children = new HashMap<>();
		this.isLeaf = false;
	}

	/**
	 * Insert a word into the trie
	 *
	 * @param word The word to be inserted in the trie
	 */
	public void insert(String word) {
		HashMap<Character, Trie> children = this.children;
		Trie t = new Trie();
		for (char c : word.toCharArray()) {
			if (children.containsKey(c)) {
				t = children.get(c);
			} else {
				t = new Trie(c);
				children.put(c, t);
			}
			children = t.children;
		}
		t.isLeaf = true;
	}

	/**
	 * Boolean value of if the trie contains this word
	 *
	 * @param word The word
	 * @return If the word is in the trie or not
	 */
	public boolean contains(String word) {
		HashMap<Character, Trie> children = this.children;
		Trie t = null;
		for (char c : word.toCharArray()) {
			if (children.containsKey(c)) {
				t = children.get(c);
				children = t.children;
			} else {
				return false;
			}
		}
		return t != null && t.isLeaf;
	}

	/**
	 * Boolean value of if the trie contains a complete word that is
	 * a prefix of my word
	 *
	 * @param word The word
	 * @return If their is a complete word that is a prefix to my word
	 */
	public boolean containsPrefix(String word) {
		HashMap<Character, Trie> children = this.children;
		Trie t = null;
		for (char c : word.toCharArray()) {
			if (children.containsKey(c)) {
				t = children.get(c);
				if (t.isLeaf) {
					return true;
				}
				children = t.children;
			} else {
				return false;
			}
		}
		return false;
	}

	/**
	 * Boolean value of if the string is a prefix of the word in the dict
	 *
	 * @param str The string
	 * @return If the str is a prefix of another word in the dict
	 */
	public boolean isPrefix(String str) {
		HashMap<Character, Trie> children = this.children;
		Trie t = null;
		for (char c : str.toCharArray()) {
			if (children.containsKey(c)) {
				t = children.get(c);
				if (t.isLeaf) {
					return false;
				}
				children = t.children;
			} else {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns the trie node in the trie using a string
	 *
	 * @param str The string
	 * @return The trie at the location of the string
	 */
	public Trie node(String str) {
		HashMap<Character, Trie> children = this.children;
		Trie t = this;
		for (char c : str.toCharArray()) {
			if (children.containsKey(c)) {
				t = children.get(c);
				children = t.children;
			} else {
				return null;
			}
		}
		return t;
	}

	/**
	 * Returns max length amoung all its children
	 *
	 * @return The max length amoung all its children
	 */
	public int maxLength() {
		int cur = 0;
		int max = 0;
		for (Trie t : children.values()) {
			cur = t.maxLength();
			if (cur > max) {
				max = cur;
			}
		}
		return max + 1;
	}
}